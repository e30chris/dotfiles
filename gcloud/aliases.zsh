alias gcl='gcloud'
alias gclconfiglist='gcloud config configurations list'
alias gclconfigdev9='gcloud config configurations activate dev9-dev'
alias gclconfigirondev='gcloud config configurations activate iron-dev'
