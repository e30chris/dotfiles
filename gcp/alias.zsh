alias gce='gcloud compute'
alias gcevmls='gcloud compute instances list'
alias gcevmdel='gcloud compute instances delete'
alias gccgetcreds='gcloud container clusters get-credentials'
